﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;
	public int Spawned;

    void Start ()
    {
        Invoke ("Spawn", spawnTime);
    }


    void Spawn ()
	{
		if (playerHealth.currentHealth <= 0f) {
			return;
		}

		int spawnPointIndex = Random.Range (0, spawnPoints.Length);

		Instantiate (enemy, spawnPoints [spawnPointIndex].position, spawnPoints [spawnPointIndex].rotation);

		Spawned += 1;

		if (Spawned == 10) {
			Spawned = 0;
			spawnTime = 0.5f;
		}
		Invoke ("Spawn", spawnTime);
	}
}
